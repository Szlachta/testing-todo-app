export default {
  'forms.taskTitle': 'Tytuł zadania',
  'forms.selectUser': 'Wybierz użytkownika',
  'forms.submit': 'Zapisz',
  'forms.submitDelete': 'Czy jesteś pewny że chcesz usunąć zadanie',
  'forms.cancel': 'Anuluj',
  'forms.delete': 'Usuń',
  'forms.queryLabel': 'Wyszukaj w tytule zadania',
  'forms.query': 'Szukana fraza',
  'forms.edit': 'Edytuj',
}
