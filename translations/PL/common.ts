export default {
  'common.darkMode': 'Zmień kolor przewodni',
  'common.closeModal': 'Zamknij modal',
  'common.completed': 'Zakońćzone',
  'common.title': 'Tytuł',
  'common.user': 'Użytkownik',
  'common.actions': 'Akcje',
  'common.prev': 'Poprzednie',
  'common.next': 'Następne',
  'common.taskUpdated': 'Zadanie zostało zaktualizowane',
  'common.taskAdded': 'Zadanie zostało dodane',
  'common.addTask': 'Dodaj zadanie',
}
