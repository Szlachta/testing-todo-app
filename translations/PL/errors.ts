export default {
  'error.404': 'Strona nie istnieje',
  'error.addTask': 'Błąd dodawania zadania',
  'error.updateTask': 'Błąd aktualizacji zadania',
  'error.updateCompleted': 'Błąd aktualizacji zadania',
  'error.deleteTask': 'Błąd usuwania zadania',
}
