import { Nullable } from '~/types/Nullable'

export interface ITask {
  userId: number
  id?: number
  title: string
  completed: boolean
}

export interface IToDoState {
  isError: boolean
  isLoading: boolean
  tasks: ITask[]
  pagginatedTasks: ITask[]
  task: Nullable<ITask>
  query: string
}

export interface ITaskPayload {
  id: number
  task: ITask
}

export interface ITaskCompletedPayload {
  id: number
  completed: boolean
}
