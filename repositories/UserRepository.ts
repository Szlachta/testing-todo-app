import axios from 'axios'
import UrlHelper from '~/helpers/urls'
import { IUser } from '~/types/UserTypes'

class UserRepository {
  public async getUsers(): Promise<IUser[]> {
    return await axios
      .get(UrlHelper.getFullApiUrl(`users`))
      .then((response) => {
        return response.data
      })
      .catch((error) => {
        throw error
      })
  }
}

export default new UserRepository()
