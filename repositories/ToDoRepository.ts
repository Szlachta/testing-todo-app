import axios from 'axios'
import UrlHelper from '~/helpers/urls'
import { ITask, ITaskCompletedPayload, ITaskPayload } from '~/types/ToDoTypes'
import notificationService from '~/store/services/NotificationService'

class ToDoRepository {
  public async getTaskLists(): Promise<ITask[]> {
    return await axios
      .get(UrlHelper.getFullApiUrl(`todos`))
      .then((response) => {
        return response.data
      })
      .catch((error) => {
        throw error
      })
  }

  public async getTask(id: number): Promise<ITask> {
    return await axios
      .get(UrlHelper.getFullApiUrl(`todos/${id}`))
      .then((response) => {
        return response.data
      })
      .catch((error) => {
        throw error
      })
  }

  public async addTask(task: ITask): Promise<ITask> {
    return await axios
      .post(UrlHelper.getFullApiUrl(`todos`), task, {
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      })
      .then((response) => {
        return response.data
      })
      .catch((error) => {
        notificationService.setNotification({
          text: 'error.addTask',
          status: 500,
        })
        throw error
      })
  }

  public async updateTask(payload: ITaskPayload): Promise<ITask> {
    return await axios
      .put(UrlHelper.getFullApiUrl(`todos/${payload.id}`), payload.task, {
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      })
      .then((response) => {
        return response.data
      })
      .catch((error) => {
        notificationService.setNotification({
          text: 'error.updateTask',
          status: 500,
        })
        throw error
      })
  }

  public async updateCompleted(payload: ITaskCompletedPayload): Promise<ITask> {
    return await axios
      .patch(
        UrlHelper.getFullApiUrl(`todos/${payload.id}`),
        {
          completed: payload.completed,
        },
        {
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
          },
        }
      )
      .then((response) => {
        return response.data
      })
      .catch((error) => {
        notificationService.setNotification({
          text: 'error.updateCompleted',
          status: 500,
        })
        throw error
      })
  }

  public async deleteTask(id: number): Promise<ITask> {
    return await axios
      .delete(UrlHelper.getFullApiUrl(`todos/${id}`))
      .then((response) => {
        return response.data
      })
      .catch((error) => {
        notificationService.setNotification({
          text: 'error.deleteTask',
          status: 500,
        })
        throw error
      })
  }
}

export default new ToDoRepository()
