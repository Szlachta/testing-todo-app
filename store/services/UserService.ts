import { Action, Module, VuexModule } from 'vuex-class-modules/commonjs'
import { Mutation } from 'vuex-class-modules'
import { store } from '~/store/store'
import { IUser, IUsersState } from '~/types/UserTypes'
import UserRepository from '~/repositories/UserRepository'

@Module
class UserService extends VuexModule {
  public usersState: IUsersState = {
    isError: false,
    isLoading: false,
    users: [],
  }

  @Mutation
  public setIsError(isError: boolean) {
    this.usersState.isError = isError
  }

  @Mutation
  public setIsLoading(isLoading: boolean) {
    this.usersState.isLoading = isLoading
  }

  @Mutation
  public setUsers(users: IUser[]) {
    this.usersState.users = users
  }

  public get isError(): boolean {
    return this.usersState.isError
  }

  public get isLoading(): boolean {
    return this.usersState.isLoading
  }

  public get users(): IUser[] {
    return this.usersState.users
  }

  @Action
  public async loadUsers(): Promise<void> {
    this.setIsLoading(true)

    await UserRepository.getUsers()
      .then((users) => {
        this.setUsers(users)
        this.setIsError(false)
      })
      .catch(() => {
        this.setUsers([])
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }
}

const userService = new UserService({
  store,
  name: 'userService',
})
export default userService
