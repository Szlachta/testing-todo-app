import { VuexModule, Module, Mutation, Action } from 'vuex-class-modules'
import { store } from '~/store/store'
import ToDoRepository from '~/repositories/ToDoRepository'
import {
  ITask,
  ITaskCompletedPayload,
  ITaskPayload,
  IToDoState,
} from '~/types/ToDoTypes'
import { Nullable } from '~/types/Nullable'

@Module
class ToDoService extends VuexModule {
  public stateToDo: IToDoState = {
    isError: false,
    isLoading: true,
    tasks: [],
    pagginatedTasks: [],
    task: null,
    query: '',
  }

  @Mutation
  public setIsError(isError: boolean) {
    this.stateToDo.isError = isError
  }

  @Mutation
  public setIsLoading(isLoading: boolean) {
    this.stateToDo.isLoading = isLoading
  }

  @Mutation
  public setTasks(tasks: ITask[]) {
    this.stateToDo.tasks = tasks
  }

  @Mutation
  public setPagginatedTasks(pagginatedTasks: ITask[]) {
    this.stateToDo.pagginatedTasks = pagginatedTasks
  }

  @Mutation
  public setTask(task: Nullable<ITask>) {
    this.stateToDo.task = task
  }

  @Mutation
  public setQuery(query: string) {
    this.stateToDo.query = query
  }

  public get isError(): boolean {
    return this.stateToDo.isError
  }

  public get isLoading(): boolean {
    return this.stateToDo.isLoading
  }

  public get tasks(): ITask[] {
    if (!this.query.length) {
      return this.stateToDo.tasks
    } else {
      return this.stateToDo.tasks.filter((task) => {
        if (!task.title) return false
        return task.title.toLowerCase().includes(this.query.toLowerCase())
      })
    }
  }

  public get pagginatedTasks(): ITask[] {
    return this.stateToDo.pagginatedTasks
  }

  public get task(): Nullable<ITask> {
    return this.stateToDo.task
  }

  public get query(): string {
    return this.stateToDo.query
  }

  @Action
  public async loadTasksLists(): Promise<void> {
    this.setIsLoading(true)

    await ToDoRepository.getTaskLists()
      .then((tasks) => {
        this.setTasks(tasks)
        this.setIsError(false)
      })
      .catch(() => {
        this.setTasks([])
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }

  @Action
  public async loadTask(id: number): Promise<void> {
    this.setIsLoading(true)

    await ToDoRepository.getTask(id)
      .then((task) => {
        this.setTask(task)
        this.setIsError(false)
      })
      .catch(() => {
        this.setTask(null)
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }

  @Action
  public async addTask(task: ITask): Promise<void> {
    this.setIsLoading(true)

    await ToDoRepository.addTask(task)
      .then(() => {
        this.setTask(null)
        this.setIsError(false)
      })
      .catch(() => {
        this.setTask(null)
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }

  @Action
  public async updateTask(payload: ITaskPayload): Promise<void> {
    this.setIsLoading(true)

    await ToDoRepository.updateTask(payload)
      .then((task) => {
        this.setTask(task)
        this.setIsError(false)
      })
      .catch(() => {
        this.setTask(null)
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }

  @Action
  public async updateCompleted(payload: ITaskCompletedPayload): Promise<void> {
    this.setIsLoading(true)

    await ToDoRepository.updateCompleted(payload)
      .then((task) => {
        this.setTask(task)
        this.setIsError(false)
      })
      .catch(() => {
        this.setTask(null)
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }

  @Action
  public async deleteTask(id: number): Promise<void> {
    this.setIsLoading(true)

    await ToDoRepository.deleteTask(id)
      .then((task) => {
        this.setTask(task)
        this.setIsError(false)
      })
      .catch(() => {
        this.setTask(null)
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }

  @Action
  public getPageTask(pageNumber: number): void {
    this.setIsLoading(true)
    const start = (pageNumber - 1) * 20
    const end = start + 20
    const tasks = this.tasks.slice(start, end)
    this.setPagginatedTasks(tasks)
  }
}

const toDoService = new ToDoService({ store, name: 'toDoService' })
export default toDoService
